from pytube import YouTube
import os
from tkinter import *

print(os.getcwd())

def download():
    video=YouTube(entryUrl.get())  
    video.streams.get_highest_resolution().download()

root=Tk()
root.geometry("490x200")
root.config(bd=15)
root.title("Video downloader")

label=Label(root, text="URL del video de Youtube\n")
label.grid(row=0, column=1)

entryUrl=Entry(root, width=50)
entryUrl.grid(row=1, column=1)

button=Button(root, text="Descargar video", width=50,  command=download)
button.grid(row=2, column=1)

root.mainloop()