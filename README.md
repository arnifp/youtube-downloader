# youtube-downloader

Script created in Python to download videos from YouTube.

## Dependencies

- [pytube](https://pytube.io/en/latest/)
- tkinter

# Run

Script in linux python3 ./downloader.py

# Make .exe for windows

Neet pyinstaller

## Run

`pyinstaller --windowed --onefile --icon=./logo.ico downloader.py`

# Ouput videos

The videos are saved in the same folder where the executable is located.
